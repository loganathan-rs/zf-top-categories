<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ziffity\TopCategories\Test\Unit\Block\Widget;

use Ziffity\TopCategories\Block\Widget\TopCategories;
use Magento\Framework\TestFramework\Unit\Helper\ObjectManager;

class TopCategoriesTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|\Magento\Catalog\Helper\Category
     */
    protected $categoryHelper;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|\Magento\Catalog\Helper\Category
     */
    protected $categoryFactory;


    protected function setUp()
    {
        $context = $this->createMock(\Magento\Framework\View\Element\Template\Context::class);
        $this->categoryHelper = $this->createMock(\Magento\Catalog\Helper\Category::class);
        $categoryFlatState = $this->createMock(\Magento\Catalog\Model\Indexer\Category\Flat\State::class);
        $this->categoryFactory = $this->getMockBuilder(\Magento\Catalog\Model\CategoryFactory::class)
            ->disableOriginalConstructor()
            ->setMethods(['load', 'create'])
            ->getMock();
            
        $topMenu = $this->createMock(\Magento\Theme\Block\Html\Topmenu::class);

        $this->block = (new ObjectManager($this))->getObject(\Ziffity\TopCategories\Block\Widget\TopCategories::class, [
            'context' => $context,
            'categoryHelper' => $this->categoryHelper,
            'categoryFlatState' => $categoryFlatState,
            'categoryFactory' => $this->categoryFactory,
            'topMenu' => $topMenu
        ]);

        $this->expectedMainTitle = 'Shop by Top Categories';
    }

    public function testgetCategoryHelper()
    {
        $this->assertEquals($this->categoryHelper, $this->block->getCategoryHelper());
    }

    public function testgetCategorymodel()
    {
        $categoryId = 5;
        $categoryMock = $this->createMock(\Magento\Catalog\Model\Category::class);
        $this->categoryFactory->expects(
            $this->once()
        )->method('load')->with(
            $categoryId
        );
        $this->assertEquals($categoryMock, $this->block->getCategorymodel($categoryId));
    }

    public function testgetMainTitle()
    {
        $this->block->setData('title', 'Shop by Top Categories');

        $this->assertEquals($this->expectedMainTitle, $this->block->getMainTitle());
    }
}
