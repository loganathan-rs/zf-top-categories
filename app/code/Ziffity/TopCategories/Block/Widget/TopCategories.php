<?php

namespace Ziffity\TopCategories\Block\Widget;

use Magento\Framework\View\Element\Template;
use Magento\Widget\Block\BlockInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Catalog\Helper\Category;
use Magento\Catalog\Model\Indexer\Category\Flat\State;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Theme\Block\Html\Topmenu;

/**
 * TopCategories - Widget returns top categories as selected by admin backend
 *
 */
class TopCategories extends Template implements BlockInterface
{

    protected $_template = 'Ziffity_TopCategories::widget/topcategories.phtml';

    /**
     * Default values
     */
    protected $_categoryHelper;
    protected $categoryFlatConfig;

    protected $topMenu;
    protected $_categoryFactory;

    protected $mainTitle;
    protected $tagLine;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Catalog\Helper\Category $categoryHelper
     * @param \Magento\Catalog\Model\Indexer\Category\Flat\State $flatState
     * @param \Magento\Catalog\Model\CategoryFactory $categoryFactory
     * @param \Magento\Theme\Block\Html\Topmenu $topMenu
     */
    public function __construct(
        Context $context,
        Category $categoryHelper,
        State $categoryFlatState,
        CategoryFactory $categoryFactory,
        Topmenu $topMenu
    ) {
        $this->_categoryHelper = $categoryHelper;
        $this->categoryFlatConfig = $categoryFlatState;
        $this->topMenu = $topMenu;
        $this->_categoryFactory = $categoryFactory;
        parent::__construct($context);
    }

    /**
     * Return categories helper
     * 
     * @return Category
     */
    public function getCategoryHelper() 
    {
        return $this->_categoryHelper;
    }
    /**
     * Return category model
     * 
     * @return CategoryFactory
     */
    public function getCategorymodel($id) 
    {
        $_category = $this->_categoryFactory->create();
        $_category->load($id);

        return $_category;
    }
    /**
     * Retrieve collection of selected categories
     * 
     * @return CategoryFactory
    */
    public function getCategoryCollection() 
    {
        $rootCat = $this->getData('parentcategory');

        $category = $this->_categoryFactory->create();
        $collection = $category
                    ->getCollection()
                    ->addAttributeToSelect('image')
                    ->addIdFilter($rootCat);

        return $collection;
    }
    /**
     * Retrieve title of block
     * 
     * @return string
    */
    public function getMainTitle() 
    {
        $mainTitle = $this->getData('title');

        return $mainTitle;
    }
}